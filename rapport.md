---
title: Calculateur réparti
author:
    - Paul HENG
    - Timothée OLIGER
date: Novembre 2018
---

# Usage

## Master

```sh
./dcalc-master <port>
```

## Node

```sh
./node [-m] [master_ip] <master_port> <node_port> <operateur>
```

## Exemple

```sh
./dcalc-master 3001
```

In other shells

```sh
./dcalc-node 127.0.0.1 3001 3010 +
./dcalc-node -m 3001 3011 "*"
./dcalc-node ::1 3001 3012 /
./dcalc-node -m 255.255.255.255 3001 3013 - ## jk don't do that
```

# Avant-propos

Ce rapport ne détaille qu'en surface les spécificités techniques, n'hésitez pas à consulter la documentation. Des manpages sont également disponibles.

```sh
make doc # necessite doxygen
```

Pour faciliter les tests, le délai pseudo-aléatoire de réponse des noeuds a été réglé relativement haut (1 - 15s). Il est possible de le réduire en modifiant la macro DELAY du fichier **common.h**.

# Implémentation

Notre implémentation de calculateur réparti utilise un système de messages formatés pour transmettre les commandes entre l'orchestrateur et les noeuds de calcul.

# Format du message

Un message est défini par son premier caractère, qui détermine alors sa signification, puis des différents arguments.

Pour les messages orcherstrateur -> noeud, on a :

* "<+|-|*|%>(a,b,c,d,...)" pour envoyer un calcul
    - exemple : "+(1,2,3)" pour 1 + 2 + 3

Le format est d'abord vérifié (syntaxe et taille pour ne pas fragmenter les paquets) par l'orchestrateur avant d'envoyer un message à un noeud.

Pour les messages noeud -> orchestrateur, on a :

* "s" (setup) pour avertir l'orchestrateur de la présence du noeud. L'orchestrateur va alors l'ajouter à sa liste de noeuds connus (sauvegarde de la struct sockaddr et de l'opérateur entre autres)
* "r\<operation\> \<resultat\>" pour envoyer le résultat d'un calcul

# Données gérées par l'orchestrateur

Quand l'orchestrateur reçoit un message de setup ("s"), il ajoute à sa liste de clients les informations suivantes :

* la struct sockaddr
* un flag **ready** pour déterminer l'état de l'orchestrateur
    - ``1`` : disponible
    - ``0`` : occupé
    - ``-1`` : down (sera remplacé si un nouveau noeud veut joindre le cluster)
* le temps auquel le noeud a été "vu" pour la dernière fois (que ce soit un keepalive, ou un retour d'opération)
* le temps auquel la dernière instruction a été envoyée (pour l'affichage du résultat)

L'orchestrateur compare périodiquement (quand il y a modification d'un des descripteurs de fichier observés par select(), ou quand il y a timeout de select()) le temps actuel et le temps auquel chaque noeud a été vu pour la dernière fois. S'il y a plus de 10 secondes de différence, le noeud est considéré comme déconnecté.

# Opérations enregistrées

De base, les opérations disponibles sont les 4 classiques (addition / soustraction / division / multiplication). Pour en ajouter d'autres, il suffit de modifier la vérification / l'obtention des paramètres pour autoriser d'autres opérateurs et de programmer l'opération dans le code des noeuds. L'opérateur doit cependant être un caractère unique, non utilisé pour d'autres commandes.

# Autres commandes

L'orchestrateur dispose de 2 commandes en plus des opérations :

* **show workers** pour afficher la liste des noeuds
    - les noeuds déconnectés sont affichés en rouge
* **exit** pour quitter le programme proprement (appel à free)

# IPv4 et IPv6 / Broadcast et Multicast

Le calculateur est compatible IPv4 et IPv6. Pour le cas de l'IPv4, l'adresse est mappée en IPv6 avec SSIT^[[SSIT](https://en.wikipedia.org/wiki/IPv6_transition_mechanism#Stateless_IP/ICMP_Translation)] (de la forme ::ffff:XXX.XXX.XXX.XXX).

Le broadcast est activé pour les noeuds, ils n'ont pas nécessairement besoin de connaitre l'IP de l'orchestrateur.

De même, un groupe de multicast est défini sur sur l'orchestrateur (``ff00::1998``). Si le multicast est supporté correctement sur le réseau, un noeud peut donc également rejoindre l'orchestrateur via l'option -m (l'ip de l'orchestrateur n'est alors pas à spécifier, et si elle l'est, elle est ignorée).
