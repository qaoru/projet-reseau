@page Nodes
# Nodes

There is no command to enter.

### How it works

A node receives a string from the orchestrator, parses it and returns the calculated value after a "short" delay.

It also sends keepalive messages periodically.
