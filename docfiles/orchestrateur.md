@page Orchestrateur
# Orchestrateur

## Commands details

### Calculations

```
<operateur>(<a>,<b>,[<c>, ...])
```

Supported operators are :

* + (addition)
* - (substrasction)
* * (multiplication)
* / (division)

### Other commands

```
show workers
```

Print the list of all nodes attached to the orchestrator. Disconnected nodes are printed in red.

```
exit
```

Exit the program cleanly (call to free for the nodes array).
