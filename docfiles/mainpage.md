# Compilation

```
make clean_all && make
```

# Usage

The calculator is split in 2 programs :

* the **orchestrator** (or master) that gets the commandes from the standard input and distributes them to the nodes
* the **nodes** (or workers, or pods #k8s) that do the computations and sends the result to the orchestrator after a pseudo-delay (between 0 and DELAY, defined in common.h, defaults to 15 sec)


How to use the @subpage Orchestrateur "orchestrator" :

```bash
./dcalc-master <port>
```

How to use the @subpage Nodes "nodes" :

```bash
./dcalc-node [-m] <orchestrator_ip> <orchestrator_port> <port> <operator>
```
