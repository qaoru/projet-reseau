/**
 * \file orchestrateur.c
 */

#if defined(__APPLE__)
#define IPV6_ADD_MEMBERSHIP IPV6_JOIN_GROUP
#define IPV6_DROP_MEMBERSHIP IPV6_LEAVE_GROUP
#endif

#include "common.h"
#include "orchestrateur.h"
#define BASE_SIZE 8

in_port_t getPort(struct sockaddr *sa) {
    if (sa->sa_family == AF_INET) {
        return (((struct sockaddr_in*)sa)->sin_port);
    }
    return (((struct sockaddr_in6*)sa)->sin6_port);
}

void getAddress(struct sockaddr *sa, char (*s)[INET6_ADDRSTRLEN]) {
    if (sa->sa_family == AF_INET) {
        inet_ntop(AF_INET,
                  &((struct sockaddr_in *)sa)->sin_addr,
                  *s,
                  INET6_ADDRSTRLEN);
    } else {
        inet_ntop(AF_INET6,
                  &((struct sockaddr_in6 *)sa)->sin6_addr,
                  *s,
                  INET6_ADDRSTRLEN);
    }
}

void print_prompt() {
    fprintf(stderr, BASE "orchestrateur> ");
}

void print_workers(nodelist list) {
    int i;
    char ip[INET6_ADDRSTRLEN];
    putchar('\n');
    for (i = 0; i < list.length; i++) {
        memset(ip, '\0', INET6_ADDRSTRLEN);
        getAddress((struct sockaddr *) &list.tab[i].w_addr, &ip);
        if (list.tab[i].ready == -1) {
            printf(RED"worker %d "YEL"%s"RED" : port %d | operator %c \n" BASE,
                   i,
                   ip,
                   ntohs(getPort((struct sockaddr *) &list.tab[i].w_addr)),
                   list.tab[i].w_operator);
        } else {
            printf("worker %d "YEL"%s"BASE" : port %d | operator %c \n",
                   i,
                   ip,
                   ntohs(getPort((struct sockaddr *) &list.tab[i].w_addr)),
                   list.tab[i].w_operator);
        }
    }
}

int getNextReady(nodelist list, int operator) {
    int i;
    for (i = 0; i < list.length; i++) {
        if((list.tab[i].w_operator == operator) && (list.tab[i].ready > 0)) {
            list.tab[i].ready = !list.tab[i].ready;
            return i;
        }
    }
    return -1;
}

time_t setReady(nodelist list, struct sockaddr *client) {
    int i;
    for (i = 0; i < (list.length); i++) {
        if(getPort((struct sockaddr *)&list.tab[i].w_addr) == getPort(client)) {
            list.tab[i].ready = !list.tab[i].ready;
            list.tab[i].last_seen = time(NULL);

            return list.tab[i].last_instruction;
        }
    }

    return 0;
}

void checkKeepAlive(nodelist *list) {
    time_t now = time(NULL);
    int i;
    for (i = 0; i < (list->length); i++) {
        // no checking for disconnected nodes
        if (list->tab[i].ready != -1 && (now - list->tab[i].last_seen > DELAY + 10)) {
            list->free_slots++;
            list->tab[i].ready = -1;
            fprintf(stderr,
                "\nNoeud %d (%c) déconnecté (timeout)\n",
                ntohs(getPort((struct sockaddr *) &(list->tab[i].w_addr))),
                list->tab[i].w_operator
            );
            print_prompt();
        }
    }
}

void updateLastSeen(nodelist list, struct sockaddr *client) {
    int i;
    for (i = 0; i < list.length; i++) {
        if(getPort((struct sockaddr *)&list.tab[i].w_addr) == getPort(client)) {
            list.tab[i].last_seen = time(NULL);
        }
    }
}

int checkParam(const char* str) {
    int psize = (int) strlen(str);

    if (psize > BUFSIZE) {
        fprintf(stderr, "input exeed buffer size\n");
        return 0;
    }
    char* p = (char*) str;

    int paramsSize = 0;

    // Define operator
    switch (*p) {
        case '+': {
            break;
        }
        case '*': {
            break;
        }
        case '-': {
            break;
        }
        case '/': {
            break;
        }
        default: {
            fprintf(stderr, "Bad operator %c\n", *p);
            return 0;
        }
    }

    p++;

    if (*p != '(' || str[psize - 2] != ')') {
        fprintf(stderr, "bad expression\n");
        return 0;
    }

    p++;

    while (*p != ')') {
        if((*p > 47 && *p < 58) || *p == '-') {
            paramsSize++;
        } else if(*p != ','){
            fprintf(stderr, "bad separator %c\n", *p);
            return 0;
        }
        p++;
    }


    if (paramsSize < 2) {
        fprintf(stderr, "Only %d params need at least 2\n", paramsSize);
        return 0;
    }
    return 1;
}

int main(const int argc, const char* argv[]) {

    /* check the number of args */
    if(argc != 2) {
        fprintf(stderr, "Usage: %s <local_port>\n", argv[0]);
	    exit(EXIT_FAILURE);

    }

    /* clients informations */
    nodelist c_list;
    if((c_list.tab = malloc(BASE_SIZE * sizeof(struct S_NODE))) == NULL) {
        perror("malloc");
        exit(EXIT_FAILURE);
    }
    c_list.length = 0;
    c_list.free_slots = 0;

    /* socket initialisation */
    int sockfd;
    socklen_t addrlen;
    struct sockaddr_in6 server;
    struct sockaddr_in6 client;
    char buf[BUFSIZE];

    time_t timer;
    char time_buffer[26];
    struct tm* tm_info;

    time_t last_instr;
    char time_buffer_instr[26];

    fd_set master;
    fd_set read_fds;

    /* Buffer pointer */
    char* bufferp;

    if((sockfd = socket(AF_INET6, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
        perror("socket");
        exit(EXIT_FAILURE);
    }

    server.sin6_family = AF_INET6;
    server.sin6_port = htons(atoi(argv[1]));
    server.sin6_addr = in6addr_any;
    server.sin6_flowinfo = (uint32_t)0;
    server.sin6_scope_id = (uint32_t)0;
    addrlen  = sizeof(struct sockaddr_in6);
    memset(buf,'\0',BUFSIZE);

    /* bind address with socket */
    if(bind(sockfd, (struct sockaddr *)&server, addrlen) == -1) {
        perror("bind");
        close(sockfd);
        exit(EXIT_FAILURE);
    }

    struct ipv6_mreq group;
    group.ipv6mr_interface = 0;
    inet_pton(AF_INET6, MULTICAST_IP, &group.ipv6mr_multiaddr);
    setsockopt(sockfd, IPPROTO_IPV6, IPV6_ADD_MEMBERSHIP, &group, sizeof group);

    FD_ZERO(&read_fds);
    FD_SET(sockfd, &master);
    FD_SET(STDIN_FILENO, &master);


    char res[] = "mSalut c'est moi le master";

    int count = 0;
    int ret;
    int n, i;
    int id_dest;
    char read_buf[BUFSIZE];
    char ip_buf[INET6_ADDRSTRLEN];
    print_prompt();

    // timeout for select()
    struct timeval timeout = {1, 0}; // 1 seconde


    for(;;) {

        read_fds = master;
        ret = select(sockfd + 1, &read_fds, NULL, NULL, &timeout);
        if (ret == -1){
            perror("select() - server" );
            exit(EXIT_FAILURE);
        }
        checkKeepAlive(&c_list);
        if (FD_ISSET(STDIN_FILENO, &read_fds)) {
            memset(read_buf, 0, BUFSIZE);
            if ((n = read(STDIN_FILENO, read_buf, BUFSIZE)) > 0) {
                if (strcmp(read_buf, "show workers\n") == 0) {
                    print_workers(c_list);
                } else if (strcmp(read_buf, "exit\n") == 0){
                    close(sockfd);
                    free(c_list.tab);
                    exit(EXIT_SUCCESS);
                } else {
                    // printf("Buffer lu : %s", read_buf); // activate to debug
                    if (checkParam(read_buf)) {
                        if ((id_dest = getNextReady(c_list,
                                                    read_buf[0])) == -1) {
                            fprintf(stderr,
                                    "Aucun noeud disponible pour %c\n",
                                    read_buf[0]);
                        } else {
                            c_list.tab[id_dest].last_instruction = time(NULL);
                            if (sendto(sockfd,
                                       read_buf, n, 0,
                                       (struct sockaddr *) &c_list.tab[id_dest],
                                       addrlen) == -1) {
                                    perror("sendto");
                                    close(sockfd);
                                    exit(EXIT_FAILURE);
                            }
                        }
                    } else {
                        fprintf(stderr, "Calcul incorrect\n");
                    }
                }

                print_prompt();
            }
            if (n == -1) {
                perror("read");
                exit(EXIT_FAILURE);
            }
        }
        if(FD_ISSET(sockfd, &read_fds)) {
            if(recvfrom(sockfd,
                        buf,
                        BUFSIZE, 0,
                        (struct sockaddr *) &client,
                        &addrlen) == -1) {
                perror("recvfrom");
                close(sockfd);
                exit(EXIT_FAILURE);
            }


	        bufferp = buf;

            if(strcmp(buf, "ping") == 0) {
                updateLastSeen(c_list, (struct sockaddr *)&client);
            } else {
                switch(buf[0]) {
                    case 's': {
                        // greeting received
                        if (!c_list.free_slots) {
                            c_list.tab[count].w_addr = (struct sockaddr_in6) client;
                            c_list.tab[count].w_operator = buf[1];
                            c_list.tab[count].ready = 1;
                            c_list.tab[count].last_seen = time(NULL);
                            count++;

                            // realloc memory if needed
                            if((++(c_list.length))%BASE_SIZE == 0) {
                                c_list.tab = realloc(c_list.tab,
                                                     (c_list.length+BASE_SIZE)*sizeof(struct S_NODE));
                                if(c_list.tab == NULL) {
                                    perror("realloc");
                                    exit(EXIT_FAILURE);
                                }
                            }
                        } else {
                            i = 0;
                            while (c_list.tab[i].ready != -1) {
                                i++;
                            }
                            c_list.tab[i].w_addr = (struct sockaddr_in6) client;
                            c_list.tab[i].w_operator = buf[1];
                            c_list.tab[i].ready = 1;
                            c_list.tab[i].last_seen = time(NULL);
                            c_list.free_slots--;
                        }

                        print_workers(c_list);
                        if(sendto(sockfd,
                                  res, sizeof(res), 0,
                                  (struct sockaddr *) &client, addrlen) == -1) {
                            perror("sendto");
                            exit(EXIT_FAILURE);
                        }
                        break;
                    }

                    case 'r': {
                        time(&timer);
                        tm_info = localtime(&timer);
                        strftime(time_buffer, 26, "%H:%M:%S", tm_info);
                        last_instr = setReady(c_list, (struct sockaddr *)&client);
                        tm_info = localtime(&last_instr);
                        strftime(time_buffer_instr, 26, "%H:%M:%S", tm_info);
                        bufferp++;
                        memset(ip_buf, '\0', INET6_ADDRSTRLEN);
                        getAddress((struct sockaddr *)&client, &ip_buf);
                        printf(YEL "\n%s " BLU "%s" BASE " by " YEL "<%s, %d>" BASE " at " MAG "%s\n" BASE,
                               time_buffer_instr,
                               bufferp,
                               ip_buf,
                               ntohs(getPort((struct sockaddr *)&client)),
                               time_buffer);
                            break;
                    }

                    default: {
                        printf("Unknown case: %c --> %s\n", buf[0], buf);
                    }
                }

                print_prompt();
            }
        }
    }

    /* close the socket */
    free(c_list.tab);
    close(sockfd);
    return 0;
}
