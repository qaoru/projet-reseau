/**
    *\file orchestrateur.h
    *\brief Structures and functions used by the orchestrator
    *\author Paul HENG - Timothée OLIGER
    */
#ifndef ORCHESTRATEUR_H
#define ORCHESTRATEUR_H
# include "common.h"

/**
 * \typedef struct node
 * represents a node : address, operator, state and last time it was seen by the orchestrator
 * a state is :
 *  - 1 : ready
 *  - 0 : in use
 *  - -1 : disconnected
 *
 */
typedef struct S_NODE {
    struct sockaddr_in6 w_addr;
    int w_operator;
    int ready;
    time_t last_seen;
    time_t last_instruction;
} node;

/**
 * \typedef struct nodelist
 * nodes array
 */
typedef struct S_NODE_TAB {
    node *tab;
    int length;
    int free_slots;
} nodelist;

/**
 * \fn in_port_t getPort(struct sockaddr *sa)
 * returns the port from a struct sockaddr (both IPv4 and IPv6 are supported)
 * \param sa pointer to a struct sockaddr
 */
in_port_t getPort(struct sockaddr *sa);

/**
 * \fn void getAddress(struct sockaddr *sa, char (*s)[INET6_ADDRSTRLEN])
 * write the address contained in a struct sockaddr in a string passed as argument
 * \param sa pointer to a struct sockaddr
 * \param s pointer to a string of size INET6_ADDRSTRLEN (max size of an IPv6)
 */
void getAddress(struct sockaddr *sa, char (*s)[INET6_ADDRSTRLEN]);

/**
 * \fn void print_workers(nodelist list)
 * prints the list of nodes(workers) attached to the orchestrator
 * \param list list of nodes
 */
void print_workers(nodelist list);

/**
 * void print_prompt()
 * prints the prompt message
 */
void print_prompt();

/**
 * \fn int getNextReady(nodelist list, int operator)
 * returns the index of the first available node for an operator
 * \param list list of nodes
 * \param operator integer corresponding to an operator
 */
int getNextReady(nodelist list, int operator);

/**
 * \fn int getNextReady(nodelist list, int operator)
 * returns the last instruction time
 * \param list list of nodes
 * \param operator integer corresponding to an operator
 */
time_t setReady(nodelist list, struct sockaddr *client);

/**
 * \fn void checkKeepAlive(nodelist *list)
 * checks if the nodes are still alive
 * a node is considered as disconnected if it has been more than 10 secondes since the last time it was seen
 * \param list list of nodes
 */
void checkKeepAlive(nodelist *list);

/**
 * \fn void updateLastSeen(nodelist list, struct sockaddr *client)
 * update the last_seen parameter of a specific node
 * \param list list of nodes
 * \param client a pointer to a struct sockaddr
 */
void updateLastSeen(nodelist list, struct sockaddr *client);

/**
 * \fn int checkParam(const char* str)
 * parses user input
 * \param str a string
 */
int checkParam(const char* str);


#endif
