/**
 * \file common.h
 * \brief common definitions
 */
#ifndef COMMON_H
#define COMMON_H
# include <sys/types.h>
# include <sys/socket.h>
# include <arpa/inet.h>
# include <netinet/ip.h>
# include <netinet/in.h>
# include <stdlib.h>
# include <string.h>
# include <stdio.h>
# include <unistd.h>
# include <sys/time.h>
# include <errno.h>
# include <time.h>

// ANSI colors
# define BASE  "\x1B[0m"
# define RED  "\x1B[31m"
# define GRN  "\x1B[32m"
# define YEL  "\x1B[33m"
# define BLU  "\x1B[34m"
# define MAG  "\x1B[35m"
# define CYN  "\x1B[36m"
# define WHT  "\x1B[37m"

// IP for multicast
# define MULTICAST_IP "ff00::1998"

// BUFSIZE have to be less than the MTU (usually 1500) to prevent packet fragmentation
# define BUFSIZE 1024

# define DELAY 20

#endif
