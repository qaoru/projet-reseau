/**
 * \file node.c
 */
#include "common.h"
#include "node.h"

/**
 * @brief Parse mathematical expression to an integer array
 *
 * @param str mathematical expression
 *
 * @return parameters
 */
 int* getParams(char* str) {
    int psize = (int) strlen(str);
    char* p = NULL;
    char buf[BUFSIZE];
    int *params, *paramsp;
    int paramsSize = 0;
    const char s[2] = ",";
    char *token;


    if (psize > BUFSIZE) {
        fprintf(stderr, "input exeed buffer size\n");
        return NULL;
    }


    // allocate params
    if ((params = calloc(BUFSIZE, sizeof(int))) == NULL) {
        fprintf(stderr, "params calloc failed");
        exit(EXIT_FAILURE);
    }

    paramsp = params;


    // using a buffer to prevent edge effect
    if (strcpy(buf, str) == NULL) {
        fprintf(stderr, "strcopy buf failed");
        exit(EXIT_FAILURE);
    }
    p = buf;

    // Define operator

    p++;

    if (*p != '(' || str[psize - 2] != ')') {
        fprintf(stderr, "bad expression\n");
        return NULL;
    }

    p++;

    // Prepare the string for split
    p[strlen(p) - 2] = '\0';

    // Split the params
    token = strtok(p, s);

    while( token != NULL ) {
        *paramsp = atoi(token);
        paramsp++;
        paramsSize++;
        token = strtok(NULL, s);
    }

    // Check Param size
    if (paramsSize < 2) {
        fprintf(stderr, "Only %d params need at least 2\n", paramsSize);
        return NULL;
    }
    return params;
}


/**
 * @brief sum
 *
 * @params params elements to sum
 *
 * @return sum
 */
int somme(int* params) {
    int res = 0;

    while (*params) {
        res += *params;
        params++;
    }

    return res;
}


/**
 * @brief subtraction
 *
 * @params params elements to substract
 *
 * @return subtraction
 */
int soustraction(int* params) {
    int res = *params;
    params++;

    while (*params) {
        res -= *params;
        params++;
    }

    return res;
}


/**
 * @brief multiplication
 *
 * @params params elements to multiply
 *
 * @return multiplication
 */
int multiplication(int* params) {
    int res = 1;

    while (*params) {
        res *= *params;
	    params++;
    }

    return res;
}


/**
 * @brief division
 *
 * @params params elements to divise
 *
 * @return division
 */
int division(int* params) {
    int res = *params;
    params++;

    while (*params) {
        res /= *params;
        params++;
    }
    return res;
}


int main(int argc, char** argv) {

    char optstring[] = "m";
    char c;
    int mflag = 0;
    while((c=getopt(argc,argv,optstring)) != EOF)
	{
		switch(c)
		{
			case 'm' : mflag = 1; break;
            case '?':
                fprintf(stderr, "usage : ./node [-m] <master_ip> <master_port> <worker_port> <operator>\n");
                exit(EXIT_FAILURE);
		}
	}
    // params test
    if (argc - optind < 4 - mflag || argc > 6) {
        fprintf(stderr, "%d - %d\n", argc - optind, argc);
        fprintf(stderr, "usage : ./node [-m] <master_ip> <master_port> <worker_port> <operator>\n");
        exit(EXIT_FAILURE);
    }

    char buf[BUFSIZE];
    int sockfd;
    char* pbuf = buf;
    socklen_t addrlen;
    struct sockaddr_in6 node;
    struct sockaddr_in6 master;
    srand(time(NULL));

    // just to treat argc correctly with -m option
    int arg_offset = (argc == 6) ? 0 : mflag;

    // variables for select()
    fd_set listener;
    fd_set read_fds;
    int ret;
    struct timeval timeout = {5, 0};

    // Calcul
    int* params;
    char res[BUFSIZE];
    int comp = 0;




    // create the socket
    if ((sockfd = socket(AF_INET6, SOCK_DGRAM, IPPROTO_UDP)) == -1 ) {
        perror("socket");
        exit(EXIT_FAILURE);
    }


    // enable broadcasting
    int enabled = 1;
    setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, &enabled, sizeof(enabled));


    FD_ZERO(&read_fds);
    FD_SET(sockfd, &listener);


    // node definition
    node.sin6_family = AF_INET6;
    node.sin6_port   = htons(atoi(argv[optind + 2 - arg_offset]));
    node.sin6_addr = in6addr_any;


    // master definition
    master.sin6_family = AF_INET6;
    master.sin6_port   = htons(atoi(argv[optind + 1 - arg_offset]));

    addrlen             = sizeof(struct sockaddr_in6);
    memset(buf, '\0', 1024);

    // address conversion
    // multicast
    if (mflag) {
        if (inet_pton(AF_INET6, MULTICAST_IP, &master.sin6_addr) != 1) {
            perror("inet_pton");
            close(sockfd);
            exit(EXIT_FAILURE);
        }
    } else {
        // trying with IPv6
        if (inet_pton(AF_INET6, argv[optind - arg_offset], &master.sin6_addr) != 1) {
            // if it fails, trying with a mapped IPv4
            char *tmp = calloc(strlen(argv[optind - arg_offset]) + 7, 1);
            if (tmp == NULL) {
                perror("calloc");
                close(sockfd);
                exit(EXIT_FAILURE);
            }
            strcat(tmp, "::ffff:");
            strcat(tmp, argv[optind - arg_offset]);
            if (inet_pton(AF_INET6, tmp, &master.sin6_addr) != 1) {
                perror("inet_pton");
                close(sockfd);
                exit(EXIT_FAILURE);
            }
            free(tmp);
        }
    }

    if (bind(sockfd, (struct sockaddr *) &node, addrlen) == -1) {
        perror("bind");
        close(sockfd);
        exit(EXIT_FAILURE);
    }


    // send greeting to the master
    buf[0] = 's';
    buf[1] = argv[optind + 3 - arg_offset][0];
    buf[2] = '\0';

    if (sendto(sockfd, buf, 3, 0, (struct sockaddr *) &master, addrlen) == -1) {
        perror("sendto");
        close(sockfd);
        exit(EXIT_FAILURE);
    }


    for(;;) {
        read_fds = listener;
        timeout.tv_sec = 5;
        timeout.tv_usec = 0;

        // wait a request
        ret = select(sockfd + 1, &read_fds, NULL, NULL, &timeout);


        // timeout, if the request is a ping
        if (ret == 0) {
            // send a keepalive packet
            if (sendto(sockfd, "ping", 5, 0, (struct sockaddr *) &master, addrlen) == -1) {
                perror("sendto");
                close(sockfd);
                exit(EXIT_FAILURE);
            }
        } else {
            // the request is a string
            if (recvfrom(sockfd, buf, BUFSIZE, 0, (struct sockaddr *) &master, &addrlen) == -1) {
                perror("recv from");
                close(sockfd);
                exit(EXIT_FAILURE);
            }


            // select the right action
            switch (buf[0]) {
                // a message
                case 'm': {
                    printf("%s\n", ++pbuf);
                    break;
                }

                // a compute
                default: {
                    params = getParams(buf);

                    if (params) {
                        switch(argv[optind + 3 - arg_offset][0]) {
                            case '+': {
                                comp = somme(params);
                                break;
                            }

                            case '-': {
                                comp = soustraction(params);
                                break;
                            }

                            case '*': {
                                comp = multiplication(params);
                                break;
                            }

                            case '/': {
                                comp = division(params);
                                break;
                            }
                        }
                        free(params);

                        // log the result
                        buf[strlen(buf) - 1] = '\0';
                        sprintf(res, "r%.1011s %d", buf, comp);
                        printf("%s : %s\n", buf, res);
                        sleep((rand()%DELAY) + 1);

                        // return the result
                        if (sendto(sockfd, res, BUFSIZE, 0, (struct sockaddr *) &master, addrlen) == -1) {
                            perror("sendto");
                            close(sockfd);
                            exit(EXIT_FAILURE);
                        }
                    }
                    else printf("%s\n", buf);
                }
            }

            // clear the buffer
            memset(buf, '\0', BUFSIZE);
        }
    }

    // close the socket
    close(sockfd);

    return EXIT_SUCCESS;
}
