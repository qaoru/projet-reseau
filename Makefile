CC = gcc -g
CFLAGS = -Wall -Werror -Wextra
INCLUDE = -I.

.PHONY: master worker test all

MASTER_DIR=master/
WORKER_DIR=worker/

MASTER_SRC=$(wildcard $(MASTER_DIR)*.c)
WORKER_SRC=$(wildcard $(WORKER_DIR)*.c)

WORKER_OBJ=$(patsubst $(WORKER_DIR)%.c, $(WORKER_DIR)%.o, $(WORKER_SRC))
MASTER_OBJ=$(patsubst $(MASTER_DIR)%.c, $(MASTER_DIR)%.o, $(MASTER_SRC))

all: master worker

test:
	echo $(WORKER_OBJ)
	echo $(MASTER_OBJ)

master: $(MASTER_OBJ)
	$(CC) $^ $(INCLUDE) -o dcalc-master

worker: $(WORKER_OBJ)
	$(CC) $^ $(INCLUDE) -o dcalc-node

%.o : %.c
	$(CC) -c $^ $(CFLAGS) $(INCLUDE) -o $@

clean:
	rm -f $(MASTER_DIR)*.o
	rm -f $(WORKER_DIR)*.o

clean_all: clean
	rm -f dcalc-master
	rm -f dcalc-node
	rm -rf html

doc:
	doxygen
